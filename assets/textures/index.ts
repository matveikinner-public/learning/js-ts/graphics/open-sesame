import { TextureLoader } from "three";
import images from "../images";

export const grass = new TextureLoader().load(images.grassImg);
export const dirt = new TextureLoader().load(images.dirtImg);
export const wood = new TextureLoader().load(images.woodImg);
export const log = new TextureLoader().load(images.logImg);
