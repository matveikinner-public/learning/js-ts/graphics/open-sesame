import grassImg from "./grass.jpeg";
import dirtImg from "./dirt.jpeg";
import woodImg from "./wood.png";
import logImg from "./log.jpeg";

const images = {
  grassImg,
  dirtImg,
  woodImg,
  logImg,
};

export default images;
