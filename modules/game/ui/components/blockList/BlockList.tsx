import { useSelector } from "react-redux";
import { selectBlocksState } from "@game/ui/adapters/redux/blocks/blocks.selectors";
import Block from "../block/Block";

const BlockList = () => {
  const blocks = useSelector(selectBlocksState);

  return blocks.map(({ key, ...rest }) => <Block key={key} {...rest} />);
};

export default BlockList;
