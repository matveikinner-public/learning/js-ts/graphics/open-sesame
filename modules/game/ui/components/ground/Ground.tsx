/* eslint-disable react/no-unknown-property */
import { NearestFilter, RepeatWrapping } from "three";
import { ThreeEvent } from "@react-three/fiber";
import { usePlane } from "@react-three/cannon";
import { useDispatch } from "react-redux";
import { values } from "ramda";
import { nanoid } from "nanoid";
import { grass as grassTexture } from "@assets/textures";
import { Block } from "@game/domain/models";
import { createBlock } from "@game/ui/adapters/redux/blocks/blocks.slice";

const Ground = () => {
  const dispatch = useDispatch();

  const [ref] = usePlane(() => ({
    rotation: [-Math.PI / 2, 0, 0],
    position: [0, -0.5, 0],
  }));

  grassTexture.magFilter = NearestFilter;
  grassTexture.wrapS = RepeatWrapping;
  grassTexture.wrapT = RepeatWrapping;
  grassTexture.repeat.set(100, 100);

  const handleCreateBlock = (e: ThreeEvent<MouseEvent>) => {
    e.stopPropagation();
    const vector = values(e.point).map((point) => Math.round(point as number)) as Block["position"];
    dispatch(createBlock({ key: nanoid(), position: vector, texture: "wood" }));
  };

  return (
    <mesh ref={ref} onClick={handleCreateBlock}>
      <planeGeometry attach="geometry" args={[100, 100]} />
      <meshStandardMaterial attach="material" map={grassTexture} />
    </mesh>
  );
};

export default Ground;
