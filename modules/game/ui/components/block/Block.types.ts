import { Block } from "@game/domain/models";

type BlockProps = Block;

export default BlockProps;
