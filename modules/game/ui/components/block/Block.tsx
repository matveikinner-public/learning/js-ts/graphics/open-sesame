/* eslint-disable react/no-unknown-property */
import { FunctionComponent } from "react";
import { NearestFilter, RepeatWrapping } from "three";
import { ThreeEvent } from "@react-three/fiber";
import { useBox } from "@react-three/cannon";
import { useDispatch } from "react-redux";
import { values } from "ramda";
import { nanoid } from "nanoid";
import * as textures from "@assets/textures";
import { Block as BlockModel } from "@game/domain/models";
import { createBlock } from "@game/ui/adapters/redux/blocks/blocks.slice";
import BlockProps from "./Block.types";

const Block: FunctionComponent<BlockProps> = ({ position, texture }: BlockProps) => {
  const dispatch = useDispatch();
  const [ref] = useBox(() => ({
    type: "Static",
    position,
  }));

  const thisTexture = textures[texture];

  thisTexture.magFilter = NearestFilter;
  thisTexture.wrapS = RepeatWrapping;
  thisTexture.wrapT = RepeatWrapping;

  const handleOnClick = (e: ThreeEvent<MouseEvent>) => {
    e.stopPropagation();
    const targetBlockFace = Math.floor(e.faceIndex! / 2);
    const { x, y, z } = ref.current!.position;

    let relativePosition = { x, y, z };

    if (targetBlockFace === 5) relativePosition = { ...relativePosition, z: z - 1 };
    if (targetBlockFace === 4) relativePosition = { ...relativePosition, z: z + 1 };
    if (targetBlockFace === 3) relativePosition = { ...relativePosition, y: y - 1 };
    if (targetBlockFace === 2) relativePosition = { ...relativePosition, y: y + 1 };
    if (targetBlockFace === 1) relativePosition = { ...relativePosition, x: x - 1 };
    if (targetBlockFace === 0) relativePosition = { ...relativePosition, x: x + 1 };

    dispatch(
      createBlock({
        key: nanoid(),
        position: values(relativePosition) as BlockModel["position"],
        texture: "wood",
      })
    );
  };

  return (
    <mesh ref={ref} onClick={handleOnClick}>
      <boxGeometry attach="geometry" />
      <meshStandardMaterial map={thisTexture} attach="material" />
    </mesh>
  );
};

export default Block;
