import { useEffect, useRef } from "react";
import { Vector3 } from "three";
import { useFrame, useThree } from "@react-three/fiber";
import { Triplet, useSphere } from "@react-three/cannon";
import useKeyboard from "@game/ui/hooks/useKeyboard/useKeyboard";
import { JUMP_FORCE, MOVEMENT_SPEED } from "./Player.constants";

/**
 * A component which represents player. In essence we attach a sphere to a player camera so that is can collide with
 * other objects
 *
 * SEQUENCE:
 * 1. Sphere (player) moves
 * 2. useEffect listens changes in sphere position and updates player position
 * 3. useFrame copies every frame player position to camera position
 */
const Player = () => {
  const { camera } = useThree();

  const [ref, api] = useSphere(() => ({
    mass: 1,
    type: "Dynamic",
    // Y-axis starts at position 1 to prevent collision between with plane on game start which results otherwise in
    // "catapult" effect where player flies at the start of the game from the collision high in the sky
    position: [0, 1, 0],
  }));

  const playerVelocity = useRef<Triplet>([0, 0, 0]);
  const playerPosition = useRef<Triplet>([0, 0, 0]);
  const playerDirection = new Vector3();

  // Subscribe to "player sphere" velocity changes. Whenever "player sphere" velocity changes update "playerVelocity"
  // vector coordinates
  useEffect(() => {
    api.velocity.subscribe((velocity) => (playerVelocity.current = velocity));
  }, [api.velocity]);

  // Subscribe to "player sphere" position changes. Whenever "player sphere" position changes update "playerPosition"
  // vector coordinates
  useEffect(() => {
    api.position.subscribe((position) => (playerPosition.current = position));
  }, [api.position]);

  // Subscribe to keyboard event listeners in the useKeyboard event hook
  const [{ moveForward, moveBackward, moveRight, moveLeft, jump }] = useKeyboard();

  const checkPlayerZAxisMovement = (zForward: boolean, zBackward: boolean) => {
    let zForwardMovement = 0;
    let zBackwardMovement = 0;

    if (zForward) zForwardMovement = 1;
    if (zBackward) zBackwardMovement = 1;

    // If the player attempts to move forward and backward at the same time the values cancel each other out
    return zBackwardMovement - zForwardMovement;
  };

  const checkPlayerXAxisMovement = (xRight: boolean, xLeft: boolean) => {
    let xRightMovement = 0;
    let xLeftMovement = 0;

    if (xRight) xRightMovement = 1;
    if (xLeft) xLeftMovement = 1;

    // If the player attempts to move right and left at the same time the values cancel each other out
    return xLeftMovement - xRightMovement;
  };

  // Every frame assign camera position to "playerPosition"
  useFrame(() => {
    camera.position.copy(new Vector3(playerPosition.current[0], playerPosition.current[1], playerPosition.current[2]));

    // Represents Z-axis i.e. movement forward and / or backward
    const zVector = new Vector3(0, 0, checkPlayerZAxisMovement(moveForward, moveBackward));

    // Represent X-axis i.e. movement right and / or left
    const xVector = new Vector3(checkPlayerXAxisMovement(moveRight, moveLeft), 0, 0);

    // 1. Subtract axios vectors to determine simulaneous movement in three dimensional axis
    // 2. Normalize vectors to have the same movement length 1
    // 3. Apply speed multiplier to normalized vectors to move faster than 1
    // 4. Apply consideration for camera movement so that our movement (X-axis / Y-axis) is always relative to the
    // camera direction
    playerDirection.subVectors(zVector, xVector).normalize().multiplyScalar(MOVEMENT_SPEED).applyEuler(camera.rotation);

    // Apply player's current movement / direction (X-axis / Y-axis) to "player sphere" velocity
    api.velocity.set(playerDirection.x, playerVelocity.current[1], playerDirection.z);

    // Check if action is to JUMP and current Y-axis velocity is smaller than 0.05 so the player is not already
    // performing the action i.e. does not have Y-axis velocity. It seems that player always has fractional velocity so
    // it is never 0. Therefore, the value is set to 0.05
    if (jump && Math.abs(playerVelocity.current[1]) < 0.05)
      api.velocity.set(playerVelocity.current[0], JUMP_FORCE, playerVelocity.current[2]);
  });

  return <mesh ref={ref}></mesh>;
};

export default Player;
