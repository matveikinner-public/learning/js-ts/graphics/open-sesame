export type KeyboardActionType = {
  moveForward: boolean;
  moveBackward: boolean;
  moveLeft: boolean;
  moveRight: boolean;
  jump: boolean;
  texture1: boolean;
  texture2: boolean;
  texture3: boolean;
};

// The lib.dom.d.ts does not provide code values as enum or otherwise so we must type it ourselves
// See https://developer.mozilla.org/en-US/docs/Web/API/UI_Events/Keyboard_event_code_values
// NOTE: Consider TypeScript module augmentation to extend lib.dom.d.ts KeyboardEvent.code on line:8996
export type KeyboardEventCodeValueType = "KeyW" | "KeyA" | "KeyS" | "KeyD" | "Space" | "Digit1" | "Digit2" | "Digit3";
