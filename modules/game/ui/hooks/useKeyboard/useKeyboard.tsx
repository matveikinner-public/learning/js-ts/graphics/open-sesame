import { useCallback, useEffect, useState } from "react";
import { KeyboardActionType, KeyboardEventCodeValueType } from "./useKeyboard.types";

const initialState: KeyboardActionType = {
  moveForward: false,
  moveBackward: false,
  moveLeft: false,
  moveRight: false,
  jump: false,
  texture1: false,
  texture2: false,
  texture3: false,
};

const actionByKey = (key: KeyboardEventCodeValueType) => {
  const keyActionMap: Record<KeyboardEventCodeValueType, keyof KeyboardActionType> = {
    KeyW: "moveForward",
    KeyS: "moveBackward",
    KeyA: "moveLeft",
    KeyD: "moveRight",
    Space: "jump",
    Digit1: "dirt",
    Digit2: "grass",
    Digit3: "wood",
  };

  return keyActionMap[key];
};

const useKeyboard = () => {
  const [actions, setActions] = useState(initialState);

  const handleKeyDown = useCallback((e: KeyboardEvent) => {
    const action = actionByKey(e.code as KeyboardEventCodeValueType);
    if (action)
      setActions((prev) => ({
        ...prev,
        [action]: true,
      }));
  }, []);

  const handleKeyUp = useCallback((e: KeyboardEvent) => {
    const action = actionByKey(e.code as KeyboardEventCodeValueType);
    if (action)
      setActions((prev) => ({
        ...prev,
        [action]: false,
      }));
  }, []);

  useEffect(() => {
    document.addEventListener("keydown", handleKeyDown);
    document.addEventListener("keyup", handleKeyUp);

    return () => {
      document.removeEventListener("keydown", handleKeyDown);
      document.removeEventListener("keyup", handleKeyUp);
    };
  }, [handleKeyDown, handleKeyUp]);

  return [actions];
};

export default useKeyboard;
