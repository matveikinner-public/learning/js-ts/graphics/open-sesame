import { Block } from "@game/domain/models";

export type BlocksState = Block[];
