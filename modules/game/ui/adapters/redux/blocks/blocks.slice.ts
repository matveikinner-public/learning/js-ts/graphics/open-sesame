import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { nanoid } from "nanoid";
import { Block } from "@game/domain/models";
import { BLOCKS_SLICE_KEY } from "./blocks.constants";
import { BlocksState } from "./blocks.types";

const initialState: BlocksState = [{ key: nanoid(), position: [1, 1, 1], texture: "wood" }];

export const blocksSlice = createSlice({
  name: BLOCKS_SLICE_KEY,
  initialState,
  reducers: {
    createBlock: (state, action: PayloadAction<Block>) => [...state, action.payload],
  },
});

export const { createBlock } = blocksSlice.actions;

export default blocksSlice.reducer;
