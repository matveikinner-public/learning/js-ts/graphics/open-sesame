import { RootState } from "@core/ui/frameworks/redux/redux.config";
import { BlocksState } from "./blocks.types";

export const selectBlocksState = (state: RootState): BlocksState => state.game.blocks;
