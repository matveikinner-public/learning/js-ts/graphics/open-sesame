import { combineReducers } from "redux";
import blocksReducer from "./blocks/blocks.slice";

export default combineReducers({
  blocks: blocksReducer,
});
