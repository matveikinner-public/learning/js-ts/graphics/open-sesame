/* eslint-disable react/no-unknown-property */
import { FunctionComponent } from "react";
import { Canvas } from "@react-three/fiber";
import { Physics } from "@react-three/cannon";
import { Sky } from "@react-three/drei";
import Ground from "../components/ground/Ground";
import Player from "../components/player/Player";
import FPV from "../components/fpv/FPV";
import Cursor from "../components/cursor/Cursor";
import BlockList from "../components/blockList/BlockList";

const GameModule: FunctionComponent = () => {
  return (
    <>
      <Canvas>
        <Sky sunPosition={[100, 100, 20]} />
        <ambientLight intensity={0.5} />
        <FPV />
        <Physics>
          <Player />
          <BlockList />
          <Ground />
        </Physics>
      </Canvas>
      <Cursor />
    </>
  );
};

export default GameModule;
