type Block = {
  key: string;
  position: [number, number, number];
  texture: BlockTexture;
};

export default Block;

export type BlockTexture = "grass" | "dirt" | "wood" | "log";
