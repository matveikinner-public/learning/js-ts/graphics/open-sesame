import { FunctionComponent, ReactNode } from "react";
import { Provider } from "react-redux";
import store from "../frameworks/redux/redux.config";

const CoreProvider: FunctionComponent<{ children: ReactNode }> = ({ children }) => {
  return <Provider store={store}>{children}</Provider>;
};

export default CoreProvider;
