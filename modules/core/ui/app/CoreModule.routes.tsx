import { lazy } from "react";
import { Navigate, RouteProps } from "react-router-dom";

const GameModule = lazy(() => import("@game/ui/app/GameModule"));

export enum CoreModuleRoutes {
  REDIRECT = "*",
  GAME_MODULE = "/",
}

const routes: RouteProps[] = [
  {
    path: CoreModuleRoutes.GAME_MODULE,
    element: <GameModule />,
  },
  {
    path: CoreModuleRoutes.REDIRECT,
    element: <Navigate replace to={CoreModuleRoutes.GAME_MODULE} />,
  },
];

export default routes;
