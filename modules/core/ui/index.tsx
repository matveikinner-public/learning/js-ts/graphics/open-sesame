import { createRoot } from "react-dom/client";
import CoreProvider from "./app/CoreProvider";
import CoreModule from "./app/CoreModule";

import "./normalize.css";
import "./index.css";

const container = document.getElementById("root")!;
const root = createRoot(container);

root.render(
  <CoreProvider>
    <CoreModule />
  </CoreProvider>
);
