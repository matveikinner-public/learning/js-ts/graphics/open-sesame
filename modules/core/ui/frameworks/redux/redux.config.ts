import { configureStore, combineReducers } from "@reduxjs/toolkit";

import gameRootReducer from "@game/ui/adapters/redux";

const rootReducer = combineReducers({
  game: gameRootReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export type AppDispatch = typeof store.dispatch;

const store = configureStore({ reducer: rootReducer, middleware: [] });

export default store;
