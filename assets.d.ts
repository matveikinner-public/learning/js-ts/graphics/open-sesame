declare module "*.jpg" {
  const JPG: string;
  export default JPG;
}

declare module "*.jpeg" {
  const JPEG: string;
  export default JPEG;
}

declare module "*.png" {
  const PNG: string;
  export default PNG;
}

// There are several options to tackle this. See links below for more information
// See https://github.com/gregberge/svgr/pull/573
// See https://github.com/gregberge/svgr/issues/546
declare module "*.svg" {
  import { SVGProps, VoidFunctionComponent } from "react";
  const SVG: VoidFunctionComponent<SVGProps<SVGSVGElement>>;
  export default SVG;
}
